// Copyright 2022 bastionm, vicentam, elmersha, nuttrand, aikosome

#include "s21_string.h"
#include <string.h>

/*-------------------
ФУНКЦИОНАЛ: КОПИРОВАНИЕ 
--------------------*/
/*-------------------
    s21_memcpy() - напрямую копирует из одной ячейки памяти в другую ячейку.
--------------------*/
void* s21_memcpy(void* dest, const void* src, size_t n) {
//  if (dest != s21_NULL) {
    char* char_dest = (char*) dest;
    char* char_src = (char*) src;

    for (size_t i = 0; i < n; i++) {
        char_dest[i] = char_src[i];
//      }
    }
    return dest;
}

/*-------------------
    s21_memmove() - копирует сначала в буфер из одной ячейки памяти. После из буфера копирует в другую ячейку.
--------------------*/
void* s21_memmove(void* dest, const void* src, size_t n) {
    // Говорим, что данные адреса теперь указываеют на тип char
    char* char_dest = (char*) dest;
    char* char_src = (char*) src;
    // создаем переменную куда будем складывать поочередно символы
    // поскольку мы не знаем где области памяти массива принимающего
    // и передающего и во избежание затирания данных во время копирования
    // создаем условие: если массив-источник сдвинут вправо по отношению
    // к массиву-назначения, то есть источник > массива - цели,
    // то помещаем данные из ячейки  char_src[i] в char_dest[i] как есть
    if (char_src > char_dest) {
        size_t i = 0;
        while (n > i) {
            char_dest[i] = char_src[i];
            i++;
        }
    // если же если массив-источник сдвинут влево по отношению
    // к массиву-назначения, то есть источник < цели,
    // курсор идет влево с конца, заполняя ячейки, пока не достигнет 0;
    } else if (char_src < char_dest) {
    while (n > 0) {
         char_dest[n-1] = char_src[n-1];
         n--;
        }
    }
    return dest;
    }


/*-------------------
    memset() - заполнение массива указанными символами.
--------------------*/
void* s21_memset(void* dest, int c, size_t len) {
    char *p = (char*) dest;
    // if (dest == s21_NULL) return s21_NULL;

    while (len > 0) {
        *p = c;
        p++;
        len--;
    }
    return p;  // Возможно необходимо обработать исключение, когда len больше длины dest
}

/*-------------------
    strcpy() - функция копирования строки из одного источника в пункт назначение
    другой строки.
--------------------*/
char* s21_strcpy(char* dest, const char* src) {
    // if (dest == s21_NULL)
    //    return s21_NULL;
    char *return_ptr = dest;
    while (*src != '\0') {
        *dest = *src;
        dest++;
        src++;
    }
    *dest = '\0';
    return return_ptr;
}

/*-------------------
    s21_strncpy() - Копирует до n символов из строки, на которую указывает src, в dest.
--------------------*/
char* s21_strncpy(char* dest, const char* src, size_t n) {
//  if (dest == s21_NULL) return s21_NULL;
    char *return_ptr = dest;
    size_t i = 0;
    while (i < n) {
        *dest = *src;
        dest++;
        src++;
        i++;
    }
    *dest = '\0';
    return return_ptr;
}

/*-------------------
ФУНКЦИОНАЛ: ПОИСК 
--------------------*/

/*-------------------
     void *s21_memchr(const void* arr, int c, size_t n) Searches for the first occurrence of the character c (an unsigned char) in the 
first n bytes of the string pointed to, by the argument str.
--------------------*/

void *s21_memchr(const void* arr, int c, size_t n) {
    void *res = s21_NULL;
    unsigned char *p = (unsigned char*)arr;
    while (n--)
        if (*p != (unsigned char)c) {
            p++;
        } else {
            res = p;
        }
    return res;
}
/*-------------------
     char *s21_strchr(const char *str, int c)   Searches for the first occurrence of the character 
     c (an unsigned char) in the string pointed to, by the argument str. 
--------------------*/
char * s21_strchr(const char * str, int c) {
     while ( *str && *str != c )
        ++str;
     return ( *str ) ? (char*) str : s21_NULL;
    }

/*
char *s21_strchr(const char *str, int c) {
    char *s21_strchr(const char *str, int c) {
        int i = 0;
        char *res;
        while(str[i] != c) {
            i++;
            if ((size_t)i == s21_strlen(str)) {
                res = s21_NULL;
                break;
            } else {
                res = (char *)str + i;
            }
        }
      return res;
    }
}*/

/*-------------------
     char *s21_strкchr(const char *str, int c)    Searches for the last occurrence of the character
      c (an unsigned char) in the string pointed to by the argument str. 
--------------------*/ 

char *s21_strrchr(const char *str, int c) {
    int i = s21_strlen(str);
    char *res = s21_NULL;
    while (str[i - 1] != c) {
        i--;
        if (i == 0) {
            res = s21_NULL;
            break;
        } else {
            res = (char *)str + i - 1;
        }
    }
  return res;
}
/*-------------------
    char *s21_strstr (const char *haystack, const char *needle) Finds the first occurrence of the entire 
    string needle (not including the terminating null character) which appears in the string haystack. 
--------------------*/ 

char *s21_strstr(const char *haystack, const char *needle) {
  int k = s21_strlen(haystack);
  int l = s21_strlen(needle);
  char *res = s21_NULL;
  if (k < l) {
      res = s21_NULL;
  } else {
      int skip[256];
      for (int i = 0; i != 256; i++) {
          skip[i] = l;
      }
      for (int i = 0; i != l; i++) {
          skip[(int) needle[i]] = l - i;
      }
      int i = l;
      while (i <= k) {
          int m = l - 1;
          int n = i - 1;
          while (needle[m] == haystack[n]) {
              if (m == 0) {
                  res = (char *) haystack + n;
                  break;
              }
              m--; n--;
          }
          i += skip[(int) haystack[i]];
          if (needle[m] != haystack[n] && m == 0 && l != 1) {
              break;
          //  res = s21_NULL;
          } else if (needle[m] == haystack[n] && l == 1) {break;}
     }
  }
    return res;
}
/*-------------------
   char *s21_strpbrk(const char *str1, const char *str2)   Finds the first character in the string str1 
   that matches any character specified in str2.  
--------------------*/ 
char *s21_strpbrk(const char *str1, const char *str2) {
    char *res = s21_NULL;
    int flag = 0;
    for (int i = 0; (size_t) i < s21_strlen(str1) && !flag; i++) {
    for (int k = 0; (size_t) k < s21_strlen(str2); k++) {
      if (str1[i] == str2[k]) {
        res = (char *) str1 + i;
          flag++;
      }
    }
  }
  return res;
}

/*-------------------
ФУНКЦИОНАЛ: СРАВНЕНИЕ 
--------------------*/

/*-------------------
   int memcmp(const void *str1, const void *str2, size_t n) - Сравнивает первые n байтов str1 и str2.
--------------------*/
int s21_memcmp(const void *str1, const void *str2, size_t n) {
    // 1) проверяем что указатели смотрят на один и тот же сектор памяти
    unsigned const char *pointer1 = str1;
    unsigned const char *pointer2 = str2;
    int Result = 0;
        for (size_t i = 0; i < n; i++) {
        if (*pointer1 != *pointer2) {  // if contents of 1 >  2, return "1", else "-1"
        //  Result = (*pointer1 >*pointer2)?((*pointer1)-(*pointer2)):
            Result = ((*pointer1) -(*pointer2));
            break;
        }
        pointer1++;
        pointer2++;
    }
    // 2) если нет, то как только встречаем несовпадение, сравниваем
    // несовпадающие сивмолы которые находятся по указанному адресу
    // через разыменование
    return Result;
}

/*-------------------
    int strcmp() Сравнивает строку, на которую указывает str1, со строкой, на которую указывает str2.
--------------------*/
int s21_strcmp(const char *str1, const char *str2) {
     int Result = 0;
      const char *pointer1 = str1;
      const char *pointer2 = str2;
      // на протяжении всей строки
         // проверяем совпадение вплоть до нулевого терминатора
     while ((*pointer1 != '\0' && *pointer2 !='\0') && *pointer1 == *pointer2) {
            pointer1++;
            pointer2++;
        }
         // если встречаем несовпадающие элементы, то приводим к (int),
         // чтоб сравнить их вес по таблице ASCII
         // сравниваем b возвращаем 1 если символ из str1
         // больше символа из str2 и -1 наборот
        //  Result = (*pointer1 == *pointer2)?0: (*pointer1 > *pointer2)?: ((*pointer1) -(*pointer2)):
        Result = ((*pointer1) -(*pointer2));
        return Result;
     }





   /*-------------------
   int strncmp(const char *str1, const char *str2, size_t n)  Сравнивает не более первых n байтов str1 и str2.
--------------------*/
int s21_strncmp(const char *str1, const char *str2, size_t n) {
    int Result = 0;
    const char *pointer1 = str1;
    const char *pointer2 = str2;
    for (size_t i = 0;  i < n; i++) {
         // проверяем совпадение вплоть до n-1

         if (*(pointer1+i)== *(pointer2+i)) {
             if (i == (n-1))
              Result = 0;
         }
         // если встречаем несовпадающие элементы, то приводим к (int),
         // чтоб сравнить их вес по таблице ASCII
         // сравниваем b возвращаем 1 если символ из str1
         // больше символа из str2 и -1 наборот
         int a1 = (int)(*(pointer1+i));
         int a2 = (int)(*(pointer2+i));
         if (a1 > a2)
        {Result = (a1-a2);
         break;}
         if (a2 > a1)
           {Result = (a1-a2);
            break;}
     }
     return Result;
}

/*-------------------
ФУНКЦИОНАЛ: ВСПОМОГАТЕЛЬНЫЕ 
--------------------*/

/*-------------------
  char *strerror(int errnum) - Выполняет поиск во внутреннем массиве номера ошибки errnum и возвращает указатель на строку 
  с сообщением об ошибке. Нужно объявить макросы, содержащие массивы сообщений об ошибке для операционных систем mac и linux. 
  Описания ошибок есть в оригинальной библиотеке. Проверка текущей ОС осуществляется с помощью директив.
--------------------*/
#ifdef __APPLE__
char* _errors[107] = {
"Undefined error: 0",
"Operation not permitted",
"No such file or directory",
"No such process",
"Interrupted system call",
"Input/output error",
"Device not configured",
"Argument list too long",
"Exec format error",
"Bad file descriptor",
"No child processes",
"Resource deadlock avoided",
"Cannot allocate memory",
"Permission denied",
"Bad address",
"Block device required",
"Resource busy",
"File exists",
"Cross-device link",
"Operation not supported by device",
"Not a directory",
"Is a directory",
"Invalid argument",
"Too many open files in system",
"Too many open files",
"Inappropriate ioctl for device",
"Text file busy",
"File too large",
"No space left on device",
"Illegal seek",
"Read-only file system",
"Too many links",
"Broken pipe",
"Numerical argument out of domain",
"Result too large",
"Resource temporarily unavailable",
"Operation now in progress",
"Operation already in progress",
"Socket operation on non-socket",
"Destination address required",
"Message too long",
"Protocol wrong type for socket",
"Protocol not available",
"Protocol not supported",
"Socket type not supported",
"Operation not supported",
"Protocol family not supported",
"Address family not supported by protocol family",
"Address already in use",
"Can't assign requested address",
"Network is down",
"Network is unreachable",
"Network dropped connection on reset",
"Software caused connection abort",
"Connection reset by peer",
"No buffer space available",
"Socket is already connected",
"Socket is not connected",
"Can't send after socket shutdown",
"Too many references: can't splice",
"Operation timed out",
"Connection refused",
"Too many levels of symbolic links",
"File name too long",
"Host is down",
"No route to host",
"Directory not empty",
"Too many processes",
"Too many users",
"Disc quota exceeded",
"Stale NFS file handle",
"Too many levels of remote in path",
"RPC struct is bad",
"RPC version wrong",
"RPC prog. not avail",
"Program version wrong",
"Bad procedure for program",
"No locks available",
"Function not implemented",
"Inappropriate file type or format",
"Authentication error",
"Need authenticator",
"Device power is off",
"Device error",
"Value too large to be stored in data type",
"Bad executable (or shared library)",
"Bad CPU type in executable",
"Shared library version mismatch",
"Malformed Mach-o file",
"Operation canceled",
"Identifier removed",
"No message of desired type",
"Illegal byte sequence",
"Attribute not found",
"Bad message",
"EMULTIHOP (Reserved)",
"No message available on STREAM",
"ENOLINK (Reserved)",
"No STREAM resources",
"Not a STREAM",
"Protocol error",
"STREAM ioctl timeout",
"Operation not supported on socket",
"Policy not found",
"State not recoverable",
"Previous owner died",
"Interface output queue is full"
};
#define ERROR_MAX 107
#endif

#ifdef __unix__
char* _errors[134] = {
"Success",
"Operation not permitted",
"No such file or directory",
"No such process",
"Interrupted syst em call",
"Input/output error",
"No such device or address",
"Argument list too long",
"Exec format error",
"Bad file descriptor",
"No child processes",
"Resource temporarily unavailable",
"Cannot allocate memory",
"Permission denied",
"Bad address",
"Block device required",
"Device or resource busy",
"File exists",
"Invalid cross-device link",
"No such device",
"Not a directory",
"Is a directory",
"Invalid argument",
"Too many open files in system",
"Too many open files",
"Inappropriate ioctl for device",
"Text file busy",
"File too large",
"No space left on device",
"Illegal seek",
"Read-only file system",
"Too many links",
"Broken pipe",
"Numerical argument out of domain",
"Numerical result out of range",
"Resource deadlock avoided",
"File name too long",
"No locks available",
"Function not implemented",
"Directory not empty",
"Too many levels of symbolic links",
"Unknown error 41",
"No message of desired type",
"Identifier removed",
"Channel number out of range",
"Level 2 not synchronized",
"Level 3 halted",
"Level 3 reset",
"Link number out of range",
"Protocol driver not attached",
"No CSI structure available",
"Level 2 halted",
"Invalid exchange",
"Invalid request descriptor",
"Exchange full",
"No anode",
"Invalid request code",
"Invalid slot",
"Unknown error 58",
"Bad font file format",
"Device not a stream",
"No data available",
"Timer expired",
"Out of streams resources",
"Machine is not on the network",
"Package not installed",
"Object is remote",
"Link has been severed",
"Advertise error",
"Srmount error",
"Communication error on send",
"Protocol error",
"Multihop attempted",
"RFS specific error",
"Bad message",
"Value too large for defined data type",
"Name not unique on network",
"File descriptor in bad state",
"Remote address changed",
"Can not access a needed shared library",
"Accessing a corrupted shared library",
".lib section in a.out corrupted",
"Attempting to link in too many shared libraries",
"Cannot exec a shared library directly",
"Invalid or incomplete multibyte or wide character",
"Interrupted system call should be restarted",
"Streams pipe error",
"Too many users",
"Socket operation on non-socket",
"Destination address required",
"Message too long",
"Protocol wrong type for socket",
"Protocol not available",
"Protocol not supported",
"Socket type not supported",
"Operation not supported",
"Protocol family not supported",
"Address family not supported by protocol",
"Address already in use",
"Cannot assign requested address",
"Network is down",
"Network is unreachable",
"Network dropped connection on reset",
"Software caused connection abort",
"Connection reset by peer",
"No buffer space available",
"Transport endpoint is already connected",
"Transport endpoint is not connected",
"Cannot send after transport endpoint shutdown",
"Too many references: cannot splice",
"Connection timed out",
"Connection refused",
"Host is down",
"No route to host",
"Operation already in progress",
"Operation now in progress",
"Stale file handle",
"Structure needs cleaning",
"Not a XENIX named type file",
"No XENIX semaphores available",
"Is a named type file",
"Remote I/O error",
"Disk quota exceeded",
"No medium found",
"Wrong medium type",
"Operation canceled",
"Required key not available",
"Key has expired",
"Key has been revoked",
"Key was rejected by service",
"Owner died",
"State not recoverable",
"Operation not possible due to RF-kill",
"Memory page has hardware error"
};
#define ERROR_MAX 134
#endif
char error1[5000];


char* s21_strrev(char* str) {
    int len = 0;
    if (!str)
        return s21_NULL;
    while (str[len] != '\0') {
        len++;
    }
    // берем 1-ый и последний и меняем местами.  Далле сближаемся к середине на одну ячейку
    // и проделываем тоже самое.
    for (int i = 0; i < (len/2); i++) {
        // int i;
        char c;
        c = str[i];
        str[i] = str[len - i - 1];
        str[len - i - 1] = c;
    }
    return str;
    }
/*------------------------
FUNCTION: _itoa()
--------------------------*/
// function convert argumet integer to string
char* s21_itoa(int i, char* strout, int base) {
    char *str = strout;
    int sign = 0;
    // Если число отрицательное переводим в положительное
    // sign = 1 знак того, что число было изменено на положительное.
    if (i < 0) {
        sign = 1;
        i *= -1;
    }
    // int digit;
    // Переводим в необходимый формат исчисления.
    // Например, base = 16 -> 16ный формат исчисления.
    while (i) {
        int digit = i % base;
        *str = (digit > 9) ? ('A' + digit - 10): '0' + digit;
        i = i / base;
        str++;
    }
    // Переводим в отрицательный знак, если число было раннее изменено на положительный
    if (sign) {
        *str++ = '-';
    }
    *str = '\0';
    // Передаем функции перевернутое число задом на перед
    s21_strrev(strout);
    return strout;
}


char *s21_strerror(int errnum)  {
    char* str = s21_NULL;
    if (errnum >= ERROR_MAX || errnum < 0) {
        char abc[10];
        s21_itoa(errnum, abc, 10);
        s21_strcpy(error1, "Unknown error: ");
        s21_strcat(error1, abc);
        str = error1;
    } else {
        str = _errors[errnum];
    }
    return str;
}


/*-------------------
    char *strtok(char *str, const char *delim)  Разбивает строку str на ряд токенов, разделенных delim.
--------------------*/
/*char * s21_strchr(const char * s, const char c) {
     while ( *s && *s != c )
        ++s;
     return ( *s ) ? (char*) s : s21_NULL;
    }*/

char* s21_strtok(char * str, const char * delim) {
    static char * c;
    if (str) {
        c = str;
//     while ( *c && s21_strchr(delim, *c) )
     //       *c++ = '\0';
    }
// if (!*c) {
     //   c = s21_NULL;
     //   } else {
    str = c;
    while ( *c && !s21_strchr(delim, *c))
        ++c;
    while ( *c && s21_strchr(delim, *c))
        *c++ = '\0';   //  }
    return str;
}
/*-------------------
   char *strcat(char *dest, const char *src)  Добавляет строку, на которую указывает src, в конец строки, на которую указывает dest.
--------------------*/

char* s21_strcat(char *dest, const char *src) {
    // проверяем указатель, если нулевой, возвращаем NULL
    if ((dest != s21_NULL) && (src != s21_NULL)) {
    // копируем указатель в курсор
    char *kursor = dest;
    // ищем окончание строки назначения
    while (*kursor != '\0') {
        kursor++;
    }
    // теперь добавляем символы из исходной строки src
    // до нулевого терминатора строки src
    while (*src != '\0') {
        *kursor++ = *src++;
    }
    // завершаем добавление нулевым терминатором
    *kursor = '\0';
    }
    return dest;
}
/*-------------------
   char *strncat(char *dest, const char *src, size_t n)  Добавляет строку, на которую указывает src, в конец строки, 
   на которую указывает dest, длиной до n символов.
--------------------*/

char* s21_strncat(char *dest, const char *src, size_t n) {
    // проверяем указатель, если нулевой, возвращаем NULL
    if ((dest != s21_NULL) && (src != s21_NULL)) {
    // копируем указатель в курсор
    char *kursor = dest;
    // ищем окончание строки назначения
    while (*kursor != '\0') {
        kursor++;
    }
    // теперь добавляем символы из исходной строки src
    // в количестве n
    for (size_t i = 0; i < n; i++) {
         *kursor++ = *src++;
    }
    // завершаем добавление нулевым терминатором
    *kursor = '\0';
    }
    return dest;
}

/*-------------------
ФУНКЦИОНАЛ: ВЫЧИСЛИТЕЛЬНЫЙ 
--------------------*/

// функция для поиска первого вхождения символа c (беззнаковый тип) в строке
// на которую указывает аргумент str. Нужна для srttok и strspn
/*char * s21_strchr(const char * str, int c) {
     while ( *str && *str != c )
        ++str;
     return ( *str ) ? (char*) str : s21_NULL;
    }*/


// функция strlen убрал + 1, чтобы не доходил до "\0"
s21_size_t s21_strlen(const char *str) {
    size_t len = 0;
    for ( ; str[len]; len++) {
    }
    return len;
}

// strcspn gives the length of the str1 part of the characters not from str2.
s21_size_t s21_strcspn(const char *str1, const char *str2) {
     s21_size_t i, j;
     for (i = 0; str1[i]; ++i) {
         for ( j = 0; str2[j]; ++j)
             if ( str1[i] == str2[j])
                 break;
         if ( str2[j])
             break;
     }
     return i;
     }

// strspn - the length of the initial segment str1, that fully equals to str2
s21_size_t s21_strspn(const char *str1, const char *str2) {
    s21_size_t len = 0;
  //  if ((str1 == s21_NULL) || (str2 == s21_NULL))
  //  len = 0;
    while (*str1 && s21_strchr(str2, *str1++)) {
        len++;
    }
    return len;
}

/*-------------------------------------------*/


/* Returns a copy of string (str) converted to uppercase. In case of any error, 
return NULL */

void *s21_to_upper(const char *str) {
    size_t len = s21_strlen(str) + 1;
    char *str_ = (char *) malloc(len * sizeof(char));
    char *res = s21_NULL;
    if (!str_) {
        res = s21_NULL;
    } else {
        for (size_t i = 0; i != len; i++) {
        if (str[i] >= 'a' && str[i] <= 'z')
            str_[i] = str[i] - 32;
        else
            str_[i] = str[i];
        }
    }
    res = str_;
    return res;
}

/* Returns a copy of string (str) converted to lowercase. In case of any error, 
return NULL */

void *s21_to_lower(const char *str) {
    size_t len = s21_strlen(str) + 1;
    char *str_ = (char *) malloc(len * sizeof(char));
    if (!str_) {
        str_ = s21_NULL;
    } else {
        for (size_t i = 0; i != len; i++) {
            if (str[i] >= 'A' && str[i] <= 'Z')
                str_[i] = str[i] + 32;
            else
                str_[i] = str[i];
        }
    }
    return str_;
}

/* Returns a new string in which a specified string (str) is inserted at a 
specified index position (start_index) in the given string (src). In case of any error,
 return NULL */

void *s21_insert(const char *src, const char *str, size_t start_index) {
    size_t len = s21_strlen(str) + s21_strlen(src);
    char *str_ = (char *)malloc(len * sizeof(char));
    if (!str_) {
        str_ = s21_NULL;
    } else {
        s21_strncpy(str_, src, start_index);
        str_[start_index] = '\0';
        s21_strcat(str_, str);
        s21_strcat(str_, src + start_index);
    }
    return str_;
}

/*------------------------
FUNCTION: _itoa()
--------------------------*/
// function convert argumet integer to string
char* s21_itoa_for_sprintf(va_list vl, char* strout, int base, \
int type_length, int accuracy_len, int sign_flag_plus, int state) {
    char *str = strout;
    int sign = 0;
    long long int simple_i = 0;
    unsigned long long int un_simple_i = 0;
    long int account_len = 0;
    int flag_lld = 0;
    if (type_length == -1) {
        simple_i = va_arg(vl, long long int);
        //printf("%lld\n", simple_i);
    } else if (type_length == 1) {
        simple_i = (short int)simple_i;
        simple_i = va_arg(vl, int);
    } else {
        simple_i = (int)simple_i;
        simple_i = va_arg(vl, int);
    }

    // Если число ноль, то добаляем сразу в строку ноль
    if (simple_i == 0 && state == STATE_PRECISION && accuracy_len == 0) {
    } else if (simple_i == 0) {
        *str = '0';
        str++;
        account_len++;
    }
    // Если число отрицательное переводим в положительное
    // sign = 1 знак того, что число было изменено на положительное.
    if (simple_i < 0) {
        sign = 1;
        if (simple_i == LLONG_MIN) {
            un_simple_i = (unsigned long long int)simple_i;
            flag_lld = 1;
        } else {
            simple_i = simple_i * -1;
        }
        //simple_i = simple_i * -1;
        //un_simple_i = (unsigned long long int)simple_i;
        //printf("%llu\n", un_simple_i);
        //printf("%d\n", 100);
    }
    
    // Переводим в необходимый формат исчисления.
    // Например, base = 16 -> 16ный формат исчисления.
    if (flag_lld == 1) {
        while (un_simple_i) {
            int digit = 0;
            digit = un_simple_i % base;
            *str = (digit > 9) ? ('A' + digit - 10): '0' + digit;
            un_simple_i = un_simple_i / base;
            str++;
            account_len++;
        }
    } else {
        while (simple_i) {
            int digit = 0;
            digit = simple_i % base;
            *str = (digit > 9) ? ('A' + digit - 10): '0' + digit;
            simple_i = simple_i / base;
            str++;
            account_len++;
        } 
    }


    // Добавляем количество оставшихся нулей указанное в "точности".
    // Если точность больше длинны строки,
    // то добавляем оставшие нули.
    if (accuracy_len > account_len) {
        accuracy_len = accuracy_len - account_len;
        while (accuracy_len) {
            *str = '0';
            str++;
            accuracy_len--;
        }
    }

    // Переводим в отрицательный знак,
    // если число было раннее изменено на положительный.
    if (sign) {
        *str++ = '-';
    } else if (sign_flag_plus == 1) {
        *str++ = '+';
    }
    *str = '\0';
    // printf("|%s|\n", str);
    // Передаем функции перевернутое число задом на перед
    s21_strrev(strout);
    // printf("|%s|\n", strout);
    return strout;
}

/*------------------------
FUNCTION: n_tu()
--------------------------*/
// Считает длину числа после точки. Данное число позволит домножить float -> int
// Например 0.22 * result => 22
/*
int n_tu(int number, int count) {
    int result = 1;
    while (count-- > 0)
        result *= number;

    return result;
} */

/*------------------------
FUNCTION: _ftoa()
--------------------------*/
// Здесь функция конвертации float to string
void s21_ftoa(va_list vl, char r[], int accuracy_len, int state, int sign_flag_plus) {
    long long int length_befor_p, length_after_p, i, \
    position, sign, add_null_after_point = 0;
    double number_float;
    unsigned long long number_int;
    int len_round = 0;
    double f = va_arg(vl, double);
    // f = round(f);
    // printf("\t%f\n", f);
    sign = 0;  // Число не меняла знак
    if (f < 0) {
        sign = 1;  // число было изменено на положительное
        f *= -1;
    }
    if (state == STATE_PRECISION) {
        f = round(f * pow(10, accuracy_len)) / pow(10, accuracy_len);
    } else {
        f = round(f * pow(10, 6)) / pow(10, 6);
    }
    // printf("%f\n", f);
    number_int = f;  // Передест только цифры до точки. остальное отрежит
    number_float = f;
    double number_float_1 = f;
    length_befor_p = 0;
    length_after_p = 0;
    
    // printf("%lld\n", number_int);
    // printf("%f\n", number_float);
    // printf("%d\n", len_round);
    /* Calculate length_after_p*/
    while (number_float - (double)number_int != 0.0 && \
    !((number_float - (double)number_int) < 0.0)) {
        number_float = f * pow(10, length_after_p + 1);
        // number_float = f * (n_tu(10.0, length_after_p + 1));
        // printf("%f\n", number_float);
        number_int = number_float;
        // printf("%ld\n", number_int);
        length_after_p++;
        len_round++;
    }
    // printf("%lld\n", length_after_p);
    if (length_after_p < 6 && state == STATE_NORMAL) {
        add_null_after_point = (6-length_after_p);
        // printf("\t%lld\n", add_null_after_point);
    }
    /* Calculate length_befor_p*/
    for (length_befor_p = (number_float_1 > 1) ? 0 : 1; number_float_1 >= 1; length_befor_p++) {
        number_float_1 /= 10;
        // printf("\t%f\n", number_float_1);
    }
    // printf("%lld\n", length_after_p);
    len_round = length_befor_p + 1 + accuracy_len;
    // printf("%lld\n", length_befor_p);
    // printf("||\t%d\n", len_round);
    // printf("%d\n", accuracy_len);
    if (length_after_p == 0 && number_int % 10 == 0 && number_float != 0) {
        position = length_befor_p;
        length_befor_p = length_befor_p + 2 + length_after_p;
    } else {
        position = length_befor_p;
        length_befor_p = length_befor_p + 2 + length_after_p;
    }
    number_float_1 = modf(f, &number_float);
    // printf("\t%f\n", f);
    // printf("\t%f\n", number_float);
    // printf("\t%f\n", number_float_1);
    // printf("\t\t%lld\n", length_befor_p);
    // printf("\t%lld\n", length_after_p);
    // printf("\t%f\n", number_float * pow(10, length_after_p+1));
    // printf("\t%f\n", number_float_1);
    number_int = number_float * pow(10, length_after_p) + number_float_1 * pow(10, length_after_p);
    // printf("\t||%llu\n",  number_int);
    // printf("\t%lld\n",  position);
    if (sign == 1 || sign_flag_plus == 1) {
        length_befor_p++;
        position++;
        len_round++;
    }
    for (i = length_befor_p-2; i >= 0; i--) {
        if (i == (length_befor_p)) {
            r[i] = '\0';
        } else if (i == position) {
            r[i] = '.';
        } else if (sign == 1 && i == 0) {
            r[i] = '-';
        } else if (sign != 1 && i == 0 && sign_flag_plus == 1) {
            r[i] = '+';
        } else {
            r[i] = (number_int % 10) + '0';
            number_int /= 10;
        }
        // printf("%c", r[i]);
        // printf(" %lld\n", i);
    }
    //printf("%d\n", state);
    // printf("||%d\n", accuracy_len);
    // printf("||%d\n", len_round);
    // printf("|%s\n", r);
    if (state == STATE_NORMAL && add_null_after_point) {
        // add null after point if length after point less than 6
        // add_null_after_point--;
        length_befor_p -= 1;
        do {
            r[length_befor_p] = '0';
            length_befor_p = length_befor_p + 1;
            add_null_after_point--;
        } while (add_null_after_point);
        r[length_befor_p] = '\0';
    } else if (state == STATE_PRECISION && accuracy_len > length_after_p) {
        add_null_after_point = accuracy_len - length_after_p;
        // printf("\t%lld\n", add_null_after_point);
        length_befor_p -= 1;
        while (add_null_after_point) {
            r[length_befor_p] = '0';
            // printf("%c", r[length_befor_p]);
            // printf(" %lld\n", length_befor_p);
            add_null_after_point--;
            length_befor_p++;
            // printf("\t%lld\n", add_null_after_point);
        }
        r[length_befor_p] = '\0';
    } else if (state == STATE_PRECISION && accuracy_len > 0) {
        // round number if we round number after point
        if (r[len_round] && r[len_round] >= '5') {
            r[len_round-1] = r[len_round-1] + 1;
        }
        //accuracy_len++;
        r[len_round] = '\0';
    } else if (state == STATE_PRECISION && accuracy_len == 0) {
        // round number if we round number befor point
        if (r[len_round] && r[len_round] >= '5') {
            r[len_round-2] = r[len_round-2] + 1;
        }
        r[len_round-1] = '\0';
    }
}
/*------------------------
FUNCTION: _utoa()
--------------------------*/
// function convert argumet unsigned integer to string
char* s21_utoa(va_list vl, char* strout, unsigned base, \
            int type_length, int accuracy_len) {
    char *str = strout;
    int account_len = 0;
    long unsigned simple_i = 0;
    if (type_length == -1) {
     // unsigned int digit;
        simple_i = va_arg(vl, long unsigned);
    } else if (type_length == 1) {
        simple_i = (short unsigned) simple_i;
        simple_i = va_arg(vl, unsigned);
    } else {
        simple_i = (unsigned) simple_i;
        simple_i = va_arg(vl, unsigned);
    }
    // Если число ноль, то добаляем сразу в строку ноль
    if (simple_i == 0) {
        *str = '0';
        str++;
        account_len++;
    }
    // Переводим в необходимый формат исчисления.
    // Например, base = 16 -> 16ный формат исчисления.
    while (simple_i) {
        unsigned int digit = simple_i % base;
        *str = (digit > 9) ? ('A' + digit - 10): '0' + digit;
        simple_i = simple_i / base;
        str++;
        account_len++;
    }
    // Добавляем количество оставшихся нулей указанное в "точности".
    // Если точность больше длинны строки,то добавляем оставшие нули.
    if (accuracy_len > account_len) {
        accuracy_len = accuracy_len - account_len;
        while (accuracy_len) {
            *str = '0';
            str++;
            accuracy_len--;
        }
    }
    // if (sign_flag_plus == 1) {
    //     *str = '+';
    //     str++;
    // }
    *str = '\0';
    // Передаем функции перевернутое число задом на перед
    s21_strrev(strout);
    // printf("%s\n", strout);
    return strout;
}

/*------------------------
FUNCTION: int _atoi(char string[]) -> stringToInteger
--------------------------*/
int s21_atoi(char string[]) {
    int j = 1, m = 0;
    for (int i = s21_strlen(string)-1; i >= 0; i--) {  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        int digit = string[i];
        digit = digit - 48;
        m = m + (digit * j);
        j = j * 10;
    }
    return m;
}

/*------------------------
FUNCTION: WIDTH. put space into buff which was write after % and return index j of buff
--------------------------*/
int s21_space_flags(int* width_len, char* tmp, char* buff, int j, int* sign_flag_minus, \
int* sign_flag_space, int sign_flag_plus, int sign_flag_del_space_if_minus, int sign_flag_str) {
    // Если есть флаг пробел и нет других флагов и ширины, то ставим пробел
    // printf("%d\n", *width_len);
    // printf("%d\n", *sign_flag_minus);
    // printf("%d\n", sign_flag_plus);
    // printf("%d\n", *sign_flag_space);
    // printf("%d\n", (int)s21_strlen(tmp));
    // Если флагоф нет и ширина не привышает длины строки, то ставим пробел
    // "ширина не привышает длины числа" - это смотрели в ftoa.
    if (sign_flag_del_space_if_minus == 1) {
        *width_len -= 1;
    } else if(sign_flag_str == 1) {
        // ignore space if type unsigned
    } else if (*width_len <= (int)s21_strlen(tmp) && *sign_flag_minus == 0 && \
    sign_flag_plus == 0 && *sign_flag_space == 1 && tmp[0] != '-') {
        buff[j] = ' ';
        // printf("%d\n", *width_len);
        j++;
    }
    // printf("%d\n", (int)s21_strlen(tmp));
    if (*tmp == '\0' && *width_len == 0) {
        //j--;
    } else if (*width_len <= (int)s21_strlen(tmp)) {
        s21_strcpy(&buff[j], tmp);
        *sign_flag_minus = 0;
        j += s21_strlen(tmp);
    } else if (*sign_flag_minus == 1) {
        s21_strcpy(&buff[j], tmp);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        j += s21_strlen(tmp);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        // Сколько ячеек останется пустыми
        int space = *width_len - s21_strlen(tmp);
        for (int z = 0; z < space; z++) {
            buff[j] = ' ';
            j++;
        }
        *sign_flag_minus = 0;
    } else if (*sign_flag_minus == 0) {
        /*Сколько ячеек останется пустыми*/
        int space = *width_len - s21_strlen(tmp);
        for (int z = 0; z < space; z++) {
            buff[j] = ' ';
            j++;
        }
        s21_strcpy(&buff[j], tmp);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        j += s21_strlen(tmp);     // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
    }
    *width_len = 0;
    return j;
}

void s21_accuracy_string_cut(char* tmp_1, char* str_arg, \
                            int accuracy_len, int state) {
    int len_arg = s21_strlen(str_arg);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
    if (state == STATE_PRECISION) {
        if (len_arg > accuracy_len && accuracy_len > 0) {
            s21_strncat(tmp_1, str_arg, accuracy_len);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        } else if (len_arg <= accuracy_len) {
            s21_strcat(tmp_1, str_arg);  // ЗАКРАФЧЕННАЯ ФУНКЦИЯ
        } else if (accuracy_len == 0) {
            tmp_1[0] = '\0';
        }
    } else if (state == STATE_NORMAL) {
        s21_strcat(tmp_1, str_arg);
    }
}

void str_to_str(char* buffer, va_list args, int type_length) {
    if (type_length == -1) {
        wchar_t *tmp = va_arg(args, wchar_t*);
        if (tmp != NULL) {
            while (*tmp) {
                *buffer++ = *tmp++;
            }
            *buffer = '\0';
        } else {
            s21_strcpy(buffer, "(null)");
        }
        printf("%s\n", buffer);
    } else {
        char *str = va_arg(args, char*);
        if (str != NULL) {
            s21_strcpy(buffer, str);
            buffer[s21_strlen(str)] = '\0';
        } else {
            buffer = "(null)";
        }
    }
}

int s21_sprintf(char* str, const char* format, ...) {
    va_list vl;
    va_start(vl, format);
    int i = 0, j = 0, tmp_id = 0;
    // char tmp[50];       // temprorary array for saving digit;
    // char* str_arg;
    //char tmp_1[600] = {'\0'};  // temprorary array for saving string;
    int a = 0;
    int b = 0;
    int c = 0;
    int* sign_flag_minus = &a;  // 1 - there is minus
    int sign_flag_plus = 0;    // 1 - there is plus
    int* sign_flag_space = &c;   // 1 - there is space
    int sign_flag_del_space_if_minus = 0; // 1 - there is minus before space
    int sign_flag_str = 0; // 1 - there is unsigned type
    int* width_len = &b;
    int accuracy_len = 0;    // For putting len which was poined after '.'
    int state = STATE_NORMAL;
    int type_length = 0;  // h = 1, l = -1, NON = 0;

    while (format && format[i]) {
        char tmp_1[600] = {'\0'};
        char tmp_width[100] = {'\0'};
        char tmp_precision[100] = {'\0'};
        char tmp[211];
        if (format[i] == '%' && format[i+1] == '%') {
            str[j] = '%';
            i++;
            j++;
        } else if (format[i] == '%') {
            i++;
            /*Check flags*/
            while (format[i] == '+' || format[i] == '-' || format[i] == ' ') {
                switch (format[i]) {
                    case '+': {
                        i++;
                        sign_flag_plus = 1;
                        break;
                    }
                    case '-': {
                        *sign_flag_minus = 1;
                        i++;
                        break;
                    }
                    case ' ': {
                        if (format[i-1] == '-') {
                            str[j] = ' ';
                            j++;
                            i++;
                            sign_flag_del_space_if_minus = 1;
                            // printf("asdasd\n");
                        } else {
                            i++;
                            *sign_flag_space = 1;
                        }

                        break;
                    }
                }
            }
            /*Check width */
            while (format[i] >= 48 && format[i] <= 57) {
                tmp_width[tmp_id] = format[i];
                tmp_id++;
                i++;
            }
            if (tmp_id > 0) {
                *width_len = s21_atoi(tmp_width);
                tmp_id = 0;
                // s21_strncpy(tmp_1, "", sizeof(tmp_1));
            }
            /*Check precision*/
            while (format[i] == '.') {
                state = STATE_PRECISION;
                i++;
                while (format[i] >= 48 && format[i] <= 57) {
                    tmp_precision[tmp_id] = format[i];
                    tmp_id++;
                    i++;
                }
                if (tmp_id > 0) {
                    accuracy_len = s21_atoi(tmp_precision);
                    tmp_id = 0;
                    // s21_strncpy(tmp_1, "", sizeof(tmp_1));
                }
            }
            /*Check length*/
            if (format[i] == 'h') {
                type_length = 1;
                i++;
            } else if (format[i] == 'l') {
                type_length = -1;
                i++;
            }

            switch (format[i]) {
                /* Convert char */
                case 'c': {
                    sign_flag_str = 1;
                    tmp_1[tmp_id] = (char)va_arg(vl, int);
                    j = s21_space_flags(width_len, tmp_1, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert decimal */
                case 'd': {
                    s21_itoa_for_sprintf(vl, tmp, 10, type_length, accuracy_len, sign_flag_plus, state);
                    // printf("|%s|\n", tmp);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert decimal */
                case 'i': {
                    s21_itoa_for_sprintf(vl, tmp, 10, type_length, accuracy_len, sign_flag_plus, state);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert unsigned int */
                case 'u': {
                    sign_flag_str = 1;
                    s21_utoa(vl, tmp, 10, type_length, accuracy_len);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert float */
                case 'f': {
                    s21_ftoa(vl, tmp, accuracy_len, state, sign_flag_plus);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert hex */
                case 'x': {
                    sign_flag_str = 1;
                    s21_itoa_for_sprintf(vl, tmp, 16, type_length, accuracy_len, sign_flag_plus, state);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /* Convert octal*/
                case 'o': {
                    sign_flag_str = 1;
                    s21_itoa_for_sprintf(vl, tmp, 8, type_length, accuracy_len, sign_flag_plus, state);
                    j = s21_space_flags(width_len, tmp, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
                /*copy string*/
                case 's': {
                    sign_flag_str = 1;
                    char buffer[100000];
                    // wchar_t *tmp = va_arg(args, wchar_t*);
                    str_to_str(buffer, vl, type_length);
                    // str_arg = va_arg(vl, char*);
                    // if (str_arg == NULL) {
                    //     str_arg = "(null)";
                    // }
                    printf("%s\n", buffer);
                    s21_accuracy_string_cut(tmp_1, buffer, accuracy_len, state);
                    j = s21_space_flags(width_len, tmp_1, str, j, sign_flag_minus, sign_flag_space, \
                    sign_flag_plus, sign_flag_del_space_if_minus, sign_flag_str);
                    break;
                }
            }
        } else {
            //printf("%c\n", format[i]);
            str[j] = format[i];
            j++;
        }
        type_length = 0;
        accuracy_len = 0;
        state = STATE_NORMAL;
        sign_flag_plus = 0;
        *sign_flag_space = 0;
        *width_len = 0;
        sign_flag_del_space_if_minus = 0;
        sign_flag_str = 0;
        //char tmp_2[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        //s21_strncpy(tmp_1, "", sizeof(tmp_1));
        // memset(tmp_1, '\0', s21_strlen(tmp_1));
        //printf("%s\n", tmp_2);
        i++;
    }
    str[j] = '\0';
    va_end(vl);
    return j;
}
