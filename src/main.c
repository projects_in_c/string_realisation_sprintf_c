#include "s21_string.h"
#include <string.h>

int main(void) {
    // char str1[1024];
    // char str2[1024];
    // s21_sprintf(str2, "%.5f", 3.5458730589043);
    // sprintf(str1, "%.5f", 3.5458730589043);
    // printf("%s\n", str1);
    // printf("%s\n", str2);

    // char buffer1[1024];
    // char buffer2[1024];
    // sprintf(buffer1, "%-20.7f", 200000.4123);
    // s21_sprintf(buffer2, "%-20.7f", 200000.4123);
    // printf("%s\n", buffer1);
    // printf("%s\n\n", buffer2);

    // sprintf(buffer1, "|%f|", 200000.4);
    // s21_sprintf(buffer2, "|%f|", 200000.4);
    // printf("%s\n", buffer1);
    // printf("%s\n\n", buffer2);

    // float b = 10000;
    // sprintf(buffer1, "%+20f", b);
    // s21_sprintf(buffer2, "%+20f", b);
    // printf("%s\n", buffer1);
    // printf("%s\n\n", buffer2);

    // char test_original[1024];
    // char test_your[1024];
    // sprintf(test_original, "Hello %s %.3f", "abc", -23.0000001);
    // s21_sprintf(test_your, "Hello %s %.3f", "abc", -23.0000001);
    // printf("%s\n", test_original);
    // printf("%s\n\n", test_your);

    // s21_sprintf(str2, "- %i - [%%- hi] - l.%i\n, [%- hi]", 0, -2, __LINE__);
    // sprintf(str1, "- %i - [%%- hi] - l.%i\n, [%- hi]", 0, -2, __LINE__ - 1);
    // printf("%s\n", str1);
    // printf("%s\n\n", str2);

    // s21_sprintf(str2, "- %i - [%%- 15hi] - l.%i\n, [%- 15hi]", 0, -2, __LINE__);
    // sprintf(str1, "- %i - [%%- 15hi] - l.%i\n, [%- 15hi]", 0, -2, __LINE__ - 1);
    // printf("%s\n", str1);
    // printf("%s\n\n", str2);


    // char *format = "%d";
    // int val = 69;
    // s21_sprintf(str1, format, 5, val);
    // sprintf(str2, format, 5, val);
    // printf("%s\n", str1);
    // printf("%s\n\n", str2);

    // char *format = "%012i";
    // int val = 69;
    // s21_sprintf(str1, format, val);
    // sprintf(str2, format, val);
    // printf("%s\n", str1);
    // printf("%s\n\n", str2);

    // char buffer3[1024];
    // char buffer4[1024];
    // int val = 0;
    // s21_sprintf(buffer3, "%.d", val);
    // sprintf(buffer4, "%.d", val);
    // printf("|%s|\n", buffer3);
    // printf("|%s|\n\n", buffer4);

    // char buffer3[1024];
    // char buffer4[1024];
    // char format[] = "%7.10f";
    // s21_sprintf(buffer3, format, 11.123456);
    // sprintf(buffer4, format, 11.123456);
    // printf("|%s|\n", buffer3);
    // printf("|%s|\n\n", buffer4);

    // char str3[512];
    // char str4[512];
    // char format[] = "% c";

    // s21_sprintf(str3, format, 'a');
    // sprintf(str4, format, 'a');
    // printf("|%s|\n", str3);
    // printf("|%s|\n\n", str4);

    // "%+2.1c%+4.2d%+5.4i%+10.2f%+55.55s%+1.1u"
    // str1, format, 'f', 21, 42, 666.666, "Lorem ipsum dolor sit amet. Aut quam ducimus.", 11

    // char str3[512];
    // char str4[512];
    // char format[] = "%+2.1c%+4.2d%+5.4i%+10.2f%+55.55s%+1.1u";
    // s21_sprintf(str3, format, 'f', 21, 42, 666.666, "Lorem ipsum dolor sit amet. Aut quam ducimus.", 11);
    // sprintf(str4, format, 'f', 21, 42, 666.666, "Lorem ipsum dolor sit amet. Aut quam ducimus.", 11);
    // printf("|%s|\n", str3);
    // printf("|%s|\n\n", str4);

    // char str8[1024];
    // char str9[1024];
    // char *str = NULL;
    // s21_sprintf(str9, "- %d - [%%3.0s] - l.%d\n, [%3.s]", -2, __LINE__ - 1, str);
    // sprintf(str8, "- %d - [%%3.0s] - l.%d\n, [%3.s]", -2, __LINE__ - 1, str);
    // printf("|%s|\n", str8);
    // printf("|%s|\n\n", str9);

    // char str1[1024];
    // char str2[1024];
    // s21_sprintf(str2, "%.5f", -3.5458730589043);
    // sprintf(str1, "%.5f", -3.5458730589043);
    // printf("|%s|\n", str1);
    // printf("|%s|\n\n", str2);


    // char str1[1024];
    // char str2[1024];
    // s21_sprintf(str2, "%s", "Hello");
    // sprintf(str1, "%ls", L"Hello");
    // printf("|%s|\n", str1);
    // printf("|%s|\n\n", str2);

    char str1[1024];
    char str2[1024];
    char *str = NULL;
    s21_sprintf(str2, "- %d - [%%-3.25s] - l.%d\n, [%-3.25s]", -2, __LINE__, str);
    sprintf(str1, "- %d - [%%-3.25s] - l.%d\n, [%-3.25s]", -2, __LINE__ - 1, str);
    printf("|%s|\n", str1);
    printf("|%s|\n\n", str2);
    return 0;
}