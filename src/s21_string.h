// Copyright 2022 bastionm, vicentam, elmersha, nuttrand, aikosome
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include <math.h>

#ifndef SRC_S21_STRING_H_
#define SRC_S21_STRING_H_
#ifdef __APPLE__
extern char* _errors[107];
#endif
#ifdef __unix__
extern char* _errors[134];
#endif
#define s21_size_t unsigned long long
#define s21_NULL ((void*)0)
extern char error1[5000];

#define is_digit(d) ((d) >= '0' && (d) <= '9')
#define STATE_NORMAL 0
#define STATE_PRECISION 1

char* s21_strrev(char* str);
char* s21_itoa(int i, char* strout, int base);
char* s21_itoa_for_sprintf(va_list vl, char* strout, int base, \
int type_length, int accuracy_len, int sign_flag_plus, int state);
// int n_tu(int number, int count);
void s21_ftoa(va_list vl, char r[], int accuracy_len, int state, int sign_flag_plus);
char* s21_utoa(va_list vl, char* strout, unsigned base, \
            int type_length, int accuracy_len);
int s21_atoi(char string[]);
int s21_space_flags(int* width_len, char* tmp, char* buff, \
int j, int* sign_flag_minus, int* sign_flag_space, int sign_flag_plus, \
int sign_flag_del_space_if_minus, int sign_flag_unsigned);
void s21_accuracy_string_cut(char* tmp_1, char* str_arg, \
                            int accuracy_len, int state);
int s21_sprintf(char* str, const char* format, ...);
void str_to_str(char* buffer, va_list args, int type_length);


int s21_memcmp(const void *str1, const void *str2, size_t n);
int s21_strcmp(const char *str1, const char *str2);
int s21_strncmp(const char *str1, const char *str2, size_t n);


char* s21_strerror(int errnum);
char* s21_strtok(char * str, const char * delim);
char* s21_strcat(char *dest, const char *src);
char* s21_strncat(char *dest, const char *src, size_t n);

s21_size_t s21_strlen(const char *str);
s21_size_t s21_strcspn(const char *str1, const char *str2);
s21_size_t s21_strspn(const char *str1, const char *str2);

void *s21_memchr(const void* arr, int c, size_t n);
char *s21_strchr(const char *str, int c);
char *s21_strrchr(const char *str, int c);
char *s21_strstr(const char *haystack, const char *needle);
char *s21_strpbrk(const char *str1, const char *str2);


void* s21_memcpy(void* dest, const void* src, size_t n);
void* s21_memmove(void* dest, const void* src, size_t n);
void* s21_memset(void* dest, int c, size_t len);
char* s21_strcpy(char* dest, const char* src);
char* s21_strncpy(char* dest, const char* src, size_t n);
// int s21_sprintf(char* str, const char* format, ...);

void *s21_to_upper(const char *str);
void *s21_to_lower(const char *str);
void *s21_insert(const char *src, const char *str, size_t start_index);



#endif  //  SRC_S21_STRING_H_

